package com.test;

import com.jayway.jsonpath.JsonPath;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class JsonApplication {

        public static final String RESOURCE_FILE = "store.json";

        public static void main(String[] args) {

            ClassLoader loader = Thread.currentThread().getContextClassLoader();
            InputStream json = loader.getResourceAsStream(RESOURCE_FILE);


            final String expression = "$.store.book[*].author";


            List<String> result;

            try {
                result = JsonPath.read(json, expression);

                if (result !=null && !result.isEmpty()){
                    for (String author : result){
                        System.out.println(author);
                    }
                }

            } catch (IOException e) {
                    e.printStackTrace();
            }
    }
}
